# codelab-service

## Getting started
A sample gitlab CI setup to use [`gcloud-compute-instance-list`](https://gitlab.com/quanzhang/codelab-component/-/blob/988be6def14e1237faa7cabce2a574a331a6d26c/templates/gcloud-compute-instance-list.yml) Gitlab Component to list GCEs available in a given project.


## Input
### Inputs: Workload Identity Federation
-   `workload_identity_provider`: (Required) The full identifier of the Workload
    Identity Provider, including the project number, pool name, and provider
    name. If provided, this must be the full identifier which includes all
    parts:

    ```text
    //iam.googleapis.com/projects/306223470399/locations/global/workloadIdentityPools/test-codelab-pool/providers/test-codelab-provider
    ```

-   `gcp_project`: (Required) The ID of the GCP project you want to interact with

    ```text
    gitlab-zhangquan
    ```